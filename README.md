# ansible-image


## Project description

Building an Ansible Docker image for personal use.

## Project goal

Create a Docker image that contains Ansible and necessary dependencies for automating configuration and managing remote hosts.
